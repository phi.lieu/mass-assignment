using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using program.Models;

namespace program.Data {
    public class programContext : DbContext {
        public programContext (
                DbContextOptions<programContext> options)
            : base(options) 
        {}

        public DbSet<Book> Book { get; set; }

        public async virtual Task AddBookAsync(Book book)
        {
            await Book.AddAsync(book);
            await SaveChangesAsync();
        }

        
    }
}
