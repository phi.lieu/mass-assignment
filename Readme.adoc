= Mass Assignment 

//tag::abstract[]

Mass Assignment (or overposting) happens when additional
parameters are accepted by the program
and mapped to data model fields that are not expected to be updated.
Mass Assignment can result in overwriting an existing sensitive field,
or adding a new property to the object.

//end::abstract[]

This vulnerability is commonly identified in programming
language frameworks where content of HTTP requests is
deserialised to a data model where all fields are binded by default.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`.
. Run security tests: `make securitytest`.

[NOTE]
--
The last test will fail.
--

=== Task 1

Navigate through the program: `http://localhost:5000`
Review the program code try to find out
why security tests fails.

[IMPORTANT]
--
Avoid looking at tests or patch file and try to
spot the security bug on your own.
--

=== Task 2

The program is vulnerable to Mass Assignment.
Perform both run-time and static analysis to test for this vulnerability.
Find out how to patch this vulnerability.

.Mass assignment run-time test
[source]
----
curl -v -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:5000/Books/Create -d "Book.Title=Malicious&Book.PrintDate=2020-01-01&Book.Price=1337&Book.isReviewed=true&__RequestVerificationToken=[FROM HTML FORM]" -b ".AspNetCore.Antiforgery.PmaILF-S5xA=[COOKIE VALUE]"
----

=== Task 3

Review `test/programSecurityTest.cs` and see how security tests
works. Review your patch from the previous task.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.1.2
* CWE 235
* https://github.com/OWASP/CheatSheetSeries/blob/a5ab44faca75dbbb2517583c337d010579d7ce81/cheatsheets/Mass_Assignment_Cheat_Sheet.md[Mass_Assignment_Cheat_Sheet.md]

//end::references[]

include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]
